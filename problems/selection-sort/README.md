# Problem
Write an efficient selection sort

# Notes
This improves on the bubble sort by only doing one swap in each pass.

## Constraints and Assumptions

## Examples

![](http://interactivepython.org/runestone/static/pythonds/_images/selectionsortnew.png)

# Pseudocode

## Approach

## Complexity Analysis

Time: O(n<sup>2</sup>)

However, since it makes fewer exhanges, it does better than bubble sort in benchmarks.

# Postmortem
