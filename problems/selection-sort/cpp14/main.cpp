#include <iostream>
#include <sstream>

using namespace std;

class Solution {
private:
    void swap(int* a, int *b) {
        int tmp = *a;
        *a = *b;
        *b = tmp;
    }

public:
    string getStr(const int nums[], size_t s) {
        string str;
        for (int i = 0; i < s; i++) {
            str.append(to_string(nums[i]));
            str.append(", ");
        }
        return str;
    }

    /**
     * Go through list in multiple passes and in each pass
     * swap the largest value to the end of the list, then
     * shrink the list by one
     */
    void sort(int nums[], size_t s) {

        while (s > 0) {
            int max_pos = 0;

            for (auto i = 0; i < s; i++) {
                if (nums[i] > nums[max_pos]) {
                    max_pos = i;
                }
            }

            swap(&nums[max_pos], &nums[s - 1]);

            cout << getStr(nums, s) << endl;
            s--;
        }
    }
};

void test1() {
    Solution s;
    int nums[] = {0};
    size_t size = sizeof(nums)/sizeof(nums[0]);
    cout << "input = " << s.getStr(nums, size) << endl;
    s.sort(nums, sizeof(nums)/sizeof(nums[0]));
    cout << "output = " << s.getStr(nums, size) << endl;
}

void test2() {
    Solution s;
    int nums[] = {1 ,2, 40, 400, 1200, 4500};
    size_t size = sizeof(nums)/sizeof(nums[0]);
    cout << "input = " << s.getStr(nums, size) << endl;
    s.sort(nums, sizeof(nums)/sizeof(nums[0]));
    cout << "output = " << s.getStr(nums, size) << endl;
}

int main() {
   int nums[] = {23, 45, 1 ,-19, 34, 1000};
   Solution s;
   size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;

   test1();
   test2();

   return 0;
}