#include <iostream>
#include <list>

using namespace std;

class MinStack {
   private:
      list<int> stack;
      int popped = INT_MIN;
   
   public:
       /** initialize your data structure here. */
       MinStack() {
           
       }
       
       void push(int x) {
         if (stack.empty()) {
            stack.push_back(x);
            return;
         }
         list<int>::iterator it = stack.begin();
         while ((*it <= x) && it != stack.end()) {
            cout << "(" << *it << ")"; 
            it++;
         }

         cout << endl;
         if (it == stack.end()) stack.push_back(x);
         //it--;
         else {
            //it--;
            stack.insert(it, x);
         }
       }
       
       void pop() {
         popped = stack.front();
         stack.pop_front();
         
       }
       
       int top() {
         if (stack.empty()) return INT_MIN;
         return stack.front();
       }
       
       int getMin() {
         if (stack.empty()) return INT_MIN;
         return stack.front();    
       }

       void dump() {
         
         for (auto e: stack) {
            cout << e << ", ";
         }
         cout << endl;
       }

};

int main() {
   MinStack obj; 
   obj.push(23);
   obj.push(1);
   obj.push(100);
   obj.dump();
   obj.pop();
   int param_3 = obj.top();
   int param_4 = obj.getMin();
   cout << "top = " << param_3 << " min = " << param_4 << endl;
   obj.dump();
   return 0;
}

