#include <iostream>
using namespace std;

class Solution {
public:
    int find(const int item, const int data[], const size_t size) {
        int low = 0;
        int high = size - 1;
        int mid = (low + high) / 2;
        while (low <= high) {
            if (data[mid] == item) return mid;
            if (item > data[mid]) low = mid + 1;
            else high = mid - 1;
            mid = (low + high) / 2;
        }

        return -1;
    }
};

int main() {
   int data[] = {3, 56, 78, 99, 100, 300, 345};
   Solution s;
   cout << s.find(23, data, sizeof(data)/sizeof(data[0])) << endl;
   cout << s.find(345, data, sizeof(data)/sizeof(data[0])) << endl;
   return 0;
}