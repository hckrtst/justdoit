//
//  main.swift
//  BinarySearch
//
//  Copyright © 2017 hckrtst. All rights reserved.
//

import Foundation

func linearSearch<T: Equatable>(_ array: [T], _ val: T ) -> Int? {
   for (i, v) in array.enumerated() where val == v {
      return i
   }
   return nil
}

let a = [34, 100, 4 ,5 ,10, 90]

if let res = linearSearch(a, 10) {
   print(res)
}
else {
   print(" 10 not found")
}

if let res = linearSearch(a, 1000) {
   print(res)
}
else {
   print("1000 not found")
}
