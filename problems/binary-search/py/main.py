#!/usr/bin/python

def solve(items, key):
    print("Got list = {}".format(items))
    if items == []:
        return False
    mid = len(items)//2
    print("Middle value = {}".format(items[mid]))
    if items[mid] == key:
        return True
    return solve(items[:mid], key) if items[mid] > key else solve(items[mid+1:], key)


if __name__ == '__main__':
    items = [3, 5 ,9, 20, 25, 30]
    print("Found 15 = {}".format(solve(items, 15)))
    print("Found 5 = {}".format(solve(items, 5)))
