package com.justdoit;

public class Main {
    public static void main(String[] args) {
        String[] a = {"abcdefg", "abcdfgrrrrr", "abc", "abcd"};
        Main m = new Main();
        System.out.println(m.longestCommonPrefix(a));

        String[] b = {};
        System.out.println(m.longestCommonPrefix(b));

    }

    private boolean isOutOfBounds(String s, int match_index) {
        return (match_index >= s.length()) ? true : false;
    }
    public String longestCommonPrefix(String[] strs) {
        // adversarial case of bad input
        if (strs.length == 0) return "";

        int match_index = -1;
        boolean matched = true;
        StringBuffer sb = new StringBuffer();
        while(true) {
            matched = true;
            match_index++;
            if (isOutOfBounds(strs[0], match_index)) break;
            char match = strs[0].charAt(match_index);
            for (String s: strs) {
                if (isOutOfBounds(s, match_index) || (s.charAt(match_index) != match)) {
                    matched = false;
                    break;
                }
            }
            if (matched) sb.append(match);
            if (!matched) break;
        }
        return sb.toString();
    }
}


