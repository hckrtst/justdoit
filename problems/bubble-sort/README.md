# Problem
Write an efficient bubble sort algorithm.

# Notes

## Constraints and Assumptions
Assume input of ints.


## Examples


# Pseudocode


## Approach


## Complexity Analysis

Time: O(N<sup>2</sup>)

Size: O(1)

# Postmortem

I'm ashamed to say that I initially passed in the following inocorrect param to `sort()`.

```cpp
s.sort(nums, sizeof(nums));
```

This causes a seg fault as it computes to 24 since each int is 4 bytes. To pass in the correct number of elements, we must do the following.


```cpp
s.sort(nums, sizeof(nums)/sizeof(nums[0]));
```

