#include <iostream>
#include <sstream>

using namespace std;

class Solution {
private:
    void swap(int* a, int *b) {
        int tmp = *a;
        *a = *b;
        *b = tmp;
    }

public:
    string getStr(const int nums[], size_t s) {
        string str;
        for (int i = 0; i < s; i++) {
            str.append(to_string(nums[i]));
            str.append(", ");
        }
        return str;
    }

    void sort(int nums[], size_t s) {
        bool swapped = true;
        while (s > 0 && swapped) {
            swapped = false;
            for (int i = 0; (i < s - 1); i++) {
                if (nums[i] > nums[i+1]) {
                    swap(&nums[i], &nums[i+1]);
                    //dump(nums, s);
                    swapped = true;
                }
            }
            s--;
        }
    }
};

void test1() {
  Solution s;
  int nums[] = {0};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

void test2() {
  Solution s;
  int nums[] = {100, 1};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

void test3() {
  Solution s;
  int nums[] = {1000, 1000000, 409, 309, -100, 1};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

void test4() {
  Solution s;
  int nums[] = {};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

int main() {
   test1();
   test2();
   test3();
   test4();

   return 0;
}