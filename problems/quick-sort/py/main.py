#!/usr/bin/python

# Solve by using mid point as pivot
def solve_with_midpoint(items):
   if len(items) < 2:
      return items
   # Use mid point as pivot
   # Then pick out the elements less than or equal to pivot and
   # throw them into smaller
   # The rest go into bigger
   # Then recurse in both sub arrays
   mid = (len(items) - 1) // 2
   pivot = items[mid]
   smaller = []
   bigger = []
   # Question:
   # Does python optimize this or do I need to
   # explicitly create a concatenated array before starting the
   # for loop
   for e in items[:mid] + items[mid+1:]:
      if e <= pivot:
         smaller.append(e)
      else:
         bigger.append(e)
   print("smaller = {}".format(smaller))
   print("bigger = {}".format(bigger))
   return solve_with_midpoint(smaller) + [pivot] + solve_with_midpoint(bigger)

def solve_with_first(items):
   if len(items) < 2:
      return items
   # Use first elem as pivot
   # Then pick out the elements less than or equal to pivot and
   # throw them into smaller
   # The rest go into bigger
   # Then recurse in both sub arrays
   pivot = items[0]
   smaller = []
   bigger = []
   # Question:
   # Does python optimize this or do I need to
   # explicitly create a concatenated array before starting the
   # for loop
   for e in items[1:]:
      if e <= pivot:
         smaller.append(e)
      else:
         bigger.append(e)
   print("smaller = {}".format(smaller))
   print("bigger = {}".format(bigger))
   return solve_with_first(smaller) + [pivot] + solve_with_first(bigger)

if __name__ == '__main__':
    print(solve_with_midpoint([3]))
    print("-------")
    print(solve_with_midpoint([12, 5 , -300, 56, 234]))
    print("**********")
    print(solve_with_first([1,2,3,4,5,6,7]))
    print("++++++++++++++")
    print(solve_with_midpoint([1,2,3,4,5,6,7]))


