#!/usr/bin/python

def count(items):
    if items == []:
        return 0
    return 1 + count(items[1:])


def solve(items):
    return count(items)

if __name__ == '__main__':
    list = [2,3,4,5,6,7,8]
    print("Nums of items in {} = {}".format(list, solve(list)))
    list = []
    print("Nums of items in {} = {}".format(list, solve(list)))
    list = ['eggs', 2, 'hello python' ,4,5,6,7,8]
    print("Nums of items in {} = {}".format(list, solve(list)))
