#include <iostream>
using namespace std;

class Solution {
public:
   string getStr(const int nums[], size_t s) {
        string str;
        for (int i = 0; i < s; i++) {
            str.append(to_string(nums[i]));
            str.append(", ");
        }
        return str;
   }

   void sort(int nums[], size_t len) {
      int counter = 0;
      for (auto i = 0; i < len; i++) {
         int k = i - 1;
         int key = nums[i];
         cout << "key = " << key << endl;

         while (k >= 0) {

            counter++;

            // if selected item is smaller than
            // the current one then keep shifting
            // items forward
            if (key <= nums[k]) {
               nums[k+1] = nums[k];
            } else {
               cout << "breaking at " << nums[k] << endl;
               break;
            }
            k--;
         }

         nums[k+1] = key;
         cout << "i = " << i << " Counter = " << counter << " : " << getStr(nums, len) << endl;
      }
   };
};

void test1() {
  Solution s;
  int nums[] = {1000, 1000000, 409, 309, -100, 1};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

void test2() {
  Solution s;
  int nums[] = {1, 10, 20, 30, 50, 1000};
  size_t size = sizeof(nums)/sizeof(nums[0]);
   cout << "input = " << s.getStr(nums, size) << endl;
   s.sort(nums, sizeof(nums)/sizeof(nums[0]));
   cout << "output = " << s.getStr(nums, size) << endl;
}

int main() {
   test1();
   test2();
   return 0;
}