# Problem
The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.

Note:
0 ≤ x, y < 2^31


[src](https://leetcode.com/problems/hamming-distance/)

# Notes

* XOR can be leveraged


## Constraints and Assumptions

## Examples

Input: x = 1, y = 4

Output: 2

Explanation:
```
1  (0 0 0 1)

4  (0 1 0 0)
```

Notice the positions where the corresponding bits are different.

# Pseudocode
```
xord = x xor y
while (xord != 0) {
   hamming = hamming + xord & 0x1
   xord = xord >> 1
}
```

## Approach

## Complexity Analysis

# Postmortem

* I seemed to have (almost) forgotten the details of XOR


