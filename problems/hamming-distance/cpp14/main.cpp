#include <iostream>
using namespace std;

class Solution {
public:
    int hammingDistance(int x, int y) {
      auto xord = x ^ y;
      auto hamming = 0;
      while (xord != 0) {
         hamming += xord & 1;
         xord >>= 1;
      }
      return hamming;
    }
};

int main() {
   Solution s;
   cout << "Hamming distance between 1 and 4 is " << s.hammingDistance(1,4) << endl;
   cout << "Hamming distance between 1 and 2147483647 is " << s.hammingDistance(1,2147483647) << endl;
   return 0;
}