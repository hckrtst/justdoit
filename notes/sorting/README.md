# Sorting

## Bubble Sort

Best time: O(n) (because we can just stop if no swaps on first pass)

Worst time: O(n<sup>2</sup>)

Makes many passes through list and always compares adjacent members.

Largest value will keep bubbling down. At each pass we reduce the list by one.

![](http://interactivepython.org/runestone/static/pythonds/_images/bubblepass.png)

```cpp

while (num > 0) {
   for (i = 0; i < (num - 1); i++) {
      if (elem[i] > elem[i+1]) {
         swap(&elem[i], &elem[i+1]);
      }
   }
   num--;
}
```

## Selection Sort

This improves on the bubble sort by only doing one swap in each pass.

![](http://interactivepython.org/runestone/static/pythonds/_images/selectionsortnew.png)

Best/Worst Time: O(n<sup>2</sup>)

However, since it makes fewer exhanges, it does better than bubble sort in benchmarks.

## Insertion Sort

Worst case: O(n<sup>2</sup>)

Best case: O(n)

## Quick Sort

## Resources
* [Sorting and Searching](http://interactivepython.org/runestone/static/pythonds/SortSearch/toctree.html)
* [Good Comparison of Sorting Algorithms](https://betterexplained.com/articles/sorting-algorithms/)
* [Analysis of algorithms](https://www.khanacademy.org/computing/computer-science/algorithms)