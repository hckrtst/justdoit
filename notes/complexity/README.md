# Analysis of algorithms

We often need to think about time/space complexity. This is described as *big Oh*.

Fastest to slowest in this order

1. O(log<sub>2</sub>n) i.e. Logarithmic time

2. O(n) i.e. Linear time

3. O(nlog<sub>2</sub>n), some algorithms like quicksort have this time

4. O(n<sup>2</sup>) a.k.a. quadratic time, for algorithms like selection sort

5. O(n!), really slow for problems like the traveling salesperson problem
