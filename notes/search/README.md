# A word about performance

* We need to frequently make trade-offs on what algorithm to use.
* Use Big-O notation to define the worst case complexity (space, time etc.).

# Linear search

If searching a list of N items, worst case time is O(N).
This is because we essentially check each element in the order in which it is stored.

# Binary search

Worst case time is O(log<sub>2</sub>(N)).

> Log is base 2, because we always halve at each step!

## Spot the trend

* 10<sup>2</sup> = 100 <=> log<sub>2</sub>100 = 2
* 10<sup>3</sup> = 1000 <=> log<sub>2</sub>1000 = 3
* 10<sup>5</sup> = 100000 <=> log<sub>2</sub>100000 = 5

## Example

If a list contains 1024 elements, we need only search 10 times. Since 2<sup>10</sup> is 1024.

For linear search, we would search 1024 elements. That's a huge savings!
