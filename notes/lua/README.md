# Lua

## Installation Notes

To get latest stable version of LuaJit `git clone http://luajit.org/git/luajit-2.0.git`.

### LuaJIT

[todo]

### LuaRocks

Luarocks is helpful for using external libraries with Lua.

### Install LuaJIT and LuaRocks

To build it from source `git clone https://github.com/luarocks/luarocks.git`

Since we are using luajit we need to configure it as follows:

```
./configure --lua-suffix=jit --with-lua-include=/usr/local/include/luajit-2.0
```

Then run

```
make build
make install
```

To use it with luajit we need to include the additional `require 'luarocks.loader'` before including any installed modules.

## Running interpretor

```
$ luajit
LuaJIT 2.0.5 -- Copyright (C) 2005-2017 Mike Pall. http://luajit.org/
JIT: ON CMOV SSE2 SSE3 SSE4.1 fold cse dce fwd dse narrow loop abc sink fuse
>
```

> Ctrl+D on *nix to exit, Ctrl+Z on windows


## Basic Types

```lua
function fact(n)
   if n == 0 then
      return 1
   else
      return n * fact(n - 1)
   end
end
```

# Resources
* https://www.lua.org/pil/contents.html
*