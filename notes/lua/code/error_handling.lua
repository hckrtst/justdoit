-- Error handling

local err, message = pcall(function() return a + "a" end)

print(tostring(err) .. '-->' .. message)

local function handle_error(x)
   print('Handling the error -->' .. x)
   return "Handled"
end

-- no error
print('------- no error ----------')
local err, handler_ret = xpcall(function(a)
   print('got ' .. tostring(a)); return true  end,
   handle_error, 22)

print(tostring(err) .. ' ~ ' .. tostring(handler_ret))

-- force error
print('---------- force error -------------')
local err, handler_ret = xpcall(function(a)
   print('got ' .. tostring(a)); return g[i]  end,
   handle_error, 22)

print(tostring(err) .. ' ~ ' .. tostring(handler_ret))