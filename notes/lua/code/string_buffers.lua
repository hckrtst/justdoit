local sock = require 'socket'
local buf = ""
local f=io.open('rfc793.txt', 'r')
local t0 = sock:gettime()
local line = f:read('*l')
while line do
    buf = buf .. line .. '\n'
    line = f:read('*l')
end
local t1 = sock:gettime()
f:close()
print(string.format('Time = %9.3f ms', (t1 - t0)*1000))

local t = {}
