local co = coroutine.create(
    function(a,b)
        print("a = " .. tostring(a) .. ", " .. "b = " .. tostring(b))
        --print(coroutine.status(co))
        coroutine.yield()
        print("resumed")
    end
)

print(coroutine.status(co))
print("calling...")
coroutine.resume(co, 1 ,3)
print(coroutine.status(co))
print("calling again...")
coroutine.resume(co, 1, 3)
print(coroutine.status(co))
print("calling yet again...")
coroutine.resume(co)
print(coroutine.status(co))
print("done.....")

print('--------------------- producer consumer pattern ----------------------')

local function send(a)
    coroutine.yield(a)
end

local producer = coroutine.create(
    function()
        while true do
            local x = io.read()
            send(x)
        end
    end
)

local function receive(prod)
    local status, value = coroutine.resume(prod)
    return value
end

local function consumer(producer)
    while true do
        local x = receive(producer)
        if not x then
            print('Bye!')
            break
        end
        io.write('>> ' .. x .. '\n')
    end
end

consumer(producer)