-- Example of producer consumer with filtering
local function receive(producer)
    local _, value = coroutine.resume(producer)
    return value
end

local function send(x)
    coroutine.yield(x)
end

local function producer()
    print('Creating producer...')
    return coroutine.create(
        function()
            while true do
                io.write('$ ')
                local x = io.read()
                send(x)
            end
        end)
end

local function filter(producer)
    print('Creating filter...')
    return coroutine.create(function()
        for line=1, math.huge do
            local x = receive(producer)
            local payload = {}
            if x then
                payload.status = 'ok'
            else
                payload.status = 'exit'
            end
            x = string.format('%5d %s', line, x)
            payload.value = x
            send(payload)
        end
    end)
end

local function consumer(producer)
    print('Starting consumer...')
    while true do
        local payload = receive(producer)
        if payload.status == 'ok' then
            io.write(payload.value, '\n')
        else
            print('Bye!')
            break
        end
    end
end

local function cleanup_and_exit()
    print('Bye!')
    os.exit()
end

consumer(filter(producer()))