local dq = {}

function dq.new()
   return {first = 0, last = -1}
end

function dq.pushfirst(list, value)
   local first = list.first - 1
   list.first = first
   list[first] = value
end

function dq.pushlast(list, value)
   local last = list.last + 1
   list.last = last
   list[last] = value
end

local function isempty(list)
   if list.first > list.last then return true end
   return false
end

function dq.popfirst(list)
   if isempty(list) then
      print('popfirst ignored for empty list')
      return
   end
   local first = list.first + 1
   list[list.first] = nil
   list.first = first
end

function dq.poplast(list)
   if isempty(list) then
      print('poplast ignored for empty list')
      return
   end
   local last = list.last - 1
   list[list.last] = nil
   list.last = last
end

local DoubleQueue = dq
return DoubleQueue