-- download webpage
require 'luarocks.loader'

local socket = require 'socket'

local function receive(connection)
   return connection:receive(2^10)
end

local function serial_download(host, file)
   local c = assert(socket.connect(host, 80))
   local count = 0
   c:send('GET ' .. file .. ' HTTP/1.0\r\n\r\n')
   local elapsed_time = 0
   while true do
      local t0 = socket.gettime()
      local s, status, partial = receive(c)
      local t1 = socket.gettime()
      elapsed_time = elapsed_time + (t1 - t0)
      io.write(s or partial)
      if status then
         print('\n\nerror status = ' .. status .. ' partial = ' .. partial)
      end
      count = count + #(s or partial)
      if status == 'closed' then
         print('Closing...')
         break
      end
   end
   c:close()
   print('\n\n\n-----------------------\nSummary\n-----------------------')
   print('File = ' .. tostring(file), ' Count ' .. tostring(count), string.format(' Time = %9.3f ms', elapsed_time*1000))
end

local function sleep(sec)
   local end_time = os.time() + sec
   repeat until os.time() > end_time
end

serial_download('www.w3.org', '/TR/html401/html40.txt')
sleep(2)
serial_download('www.w3.org', '/no_such_file.txt')