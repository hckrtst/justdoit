local function print_result(a)
    for _,v in ipairs(a) do
        io.write(v .. ', ')
    end
    print('')
end

-- generate all permutations of a given array
local function permgen(a, n)
    n = n or #a
    if n <= 1 then
        print_result(a)
    else
        for i=1,n do
            -- swap i-th elem with n-th
            a[n], a[i] = a[i], a[n]
            -- generate all permutations
            permgen(a, n-1)
            -- restore elem
            a[n], a[i] = a[i], a[n]
        end
    end
end

-- permutations via coroutines
local function permutation_generator(a, n)
    n = n or #a
    if n <= 1 then
        coroutine.yield(a)
    else
        for i = 1,n do
            a[n], a[i] = a[i], a[n]
            permutation_generator(a, n-1)
            a[n], a[i] = a[i], a[n]
        end
    end
end
local function permutations(a)
    local co = coroutine.create(function () permutation_generator(a) end)
    return function()
        local _, res = coroutine.resume(co)
        return res
    end
end

--a = {2, 1, 5, 'hello', 4.3454}
--permgen(a)

for p in permutations({'a', 'b', 'c'}) do
    print_result(p)
end