local dq = require 'DoubleQueue'
local li = dq.new()
dq.poplast(li)
dq.pushfirst(li, 20)
dq.pushlast(li, 300)
dq.pushlast(li, 4000)

for k,v in pairs(li) do
    print(k .. ' --> ' .. v)
end
