-- Bags are multisets: an item may appear multiple times

local function insert(bag, element)
    bag[element] = (bag[element] or 0) + 1
end

local function remove(bag, element)
    local count  = bag[element]
    bag[element] = (count and count > 1) and count - 1 or nil
end

bag = {}
insert(bag, 45)
insert(bag, 45)
insert(bag, 'lua')

print('------------- insert ----------------')
for k,v in pairs(bag) do
    print(k .. ' --> ' ..v)
end

remove(bag, 67)
remove(bag,45)
print('------------- remove ----------------')
for k,v in pairs(bag) do
    print(k .. ' --> ' ..v)
end