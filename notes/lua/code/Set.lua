local function allwords()
    local line = io.read()
    local pos = 1
    return function()
        while line do
            local s,e = string.find(line, '%w+', pos) -- find a word
            if s then
                pos = e + 1 -- position after word
                return string.sub(line, s ,e) -- return current word
            else
                line = io.read()
                pos = 1 -- reset position to start of line
            end
        end
        return nil
    end
end

local function Set(list)
    local set = {}
    for _,v in ipairs(list) do set[v] = true end
    return set
end

local reserved = Set{'while', 'end', 'function', 'local', 'if'}

local function fwrite(format, ...)
    io.write(string.format(format, ...))
end

for w in allwords() do
    if not reserved[w] then
        fwrite('\n\'%s\' is allowed :)\n', w)
    else
        fwrite('\n\'%s\' is reserved :(\n', w)
    end
end