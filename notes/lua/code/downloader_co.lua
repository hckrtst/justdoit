-- download webpage
--require 'luarocks.loader'

-- local vars
local socket = require 'socket'
threads = {}

-- local functions

local function head(s)
   return s:sub(1, 600)
end

local function receive_nonblocking(conn, token)
   --print('receiving...')
   conn:settimeout(0) -- make non-blocking
   local s, status, partial = conn:receive(2^10)
   --print(string.format('status = %s', status))
   if status == 'timeout' then
      --print('yield ' .. token)
      coroutine.yield(conn)
      --print('resume ' .. token)
   end
   --print(string.format('status = %s, s = %s, partial = %s', status, s, partial))
   return s or partial, status
end

local function download(host, file, token)
   -- make an ssl connection
   local c = assert(socket.connect(host, 80))
   print(string.format('Connected to %s', host))
   local count = 0
   c:send(string.format('GET %s HTTP/1.1\r\nHost: %s\r\n\r\n', file, host))
   local elapsed_time = 0
   print(string.format('downloading %s for %s...', file, token))
   local buf = ''
   while true do
      local t0 = socket.gettime()
      local s, status = receive_nonblocking(c, token)
      local t1 = socket.gettime()
      elapsed_time = elapsed_time + (t1 - t0)
      --print(s)
      --io.write(s or partial)
      -- if status then
      --    print(string.format('\n\nerror status = %s, partial = %s', tostring(status), tostring(partial)))
      -- end
      -- if s then print(s) end
      if s then buf = buf .. tostring(s) end
      count = count + #s
      if status == 'closed' then
         print('Closing...')
         print(head(buf))
         break
      end
   end
   c:close()
   print('\n\n\n-----------------------\nSummary\n-----------------------')
   print('File = ' .. tostring(file), ' Count ' .. tostring(count), string.format(' Time = %9.3f ms', elapsed_time*1000))
end

local function get(host, file, token)
   local co = coroutine.create(
      function ()
         download(host, file, token)
      end)
   table.insert(threads, co)
end

local function dispatch()
   local i = 1
   local bail = 4
   while true do
      if threads[i] == nil then
         if threads[1] == nil then break end
         i = 1
      end
      --print('resuming ' .. tostring(threads[i]))
      local status, res = coroutine.resume(threads[i])
      --print(string.format('status = %s, res = %s', status, res))
      if not status then
         print(string.format('cleaning up %s', threads[i]))
         table.remove(threads, i)
      else
         i = i + 1
      end
   end
end



-- Test
host = 'www.w3.org'
--get(host, '/TR/html401/html40.txt', 'dizzy')
get('duckduckgo.com', '/index.html', 'fuzzy')
get('lua-users.org', '/index.html', 'wuzzy')
--get(host, '/no_such_file.txt', 'noonie')

-- host = 'www.lua.org'
-- get(host, '/license.html', 'rio')
-- get(host, '/license.html', 'madrid')
dispatch()


