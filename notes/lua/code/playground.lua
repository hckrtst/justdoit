a = 100

print(a)

local a = 20

print(a)

print(b)

local b = 'brazil'

print(b)

b = 1000

print(b)

-- beware type conversions
print('Beware type coercions!')
print(10 .. "100")
print(10 + "100")

print('Use tostring() for explicit conversions')
print(tostring(10000.3456))

print("Another way to coerce to strings")
print(10 .. "" == "10")

print('Tables are the only data structure mechanism you will need!')
a = {}
a['first_name'] = 'Sherlock'
a['last_name'] = 'Holmes'
a['address'] = '221 b baker st.'

print(a)
for k,v in pairs(a) do
   print(k .. '-->' .. v)
end

print('Tables are anonymous')
b = a
print(b['last_name'])
print(b)

print('pairs() iterates over all key value pairs')
for k,v in pairs(a) do
   print(k)
end


t = {x = 101, y = 102}
t[1] = 'One'
t.z = 'Zee?'

print(t['z'] .. ' is same as ' .. t.z)

-- Temporarily comment out
-- print('We can use tables to make a linked list')
-- print('Enter some values now...press Ctrl+D/Z to end')
-- list = nil
-- for line in io.lines() do
--    list = {next=list, value=line}
-- end
-- l = list
-- while l do
--    print(l.value)
--    l = l.next
-- end

print('ipairs() iterates over keys that are indices')
a[1] = 'One'
for k,v in ipairs(a) do
   print(k .. '->' .. v)
end
print(a[0])
a[10] = 'Ten'
print(a[10])

print('We can mix record and list styles')
data = {id = 12, {a = 34, b = 46}, foo = 'bar', [0] = 'Zero'}
data[2] = 'hellooo´'
print('----- Use pairs() -----')
for k,v in pairs(data) do
   if type(v) == 'table' then
      for k,v in pairs(v) do
         print(k .. '---->' .. v)
      end
   else
      print(k .. '-->' .. v)
   end
end
print('----- Use ipairs() ------')
for k,v in ipairs(data) do
   if type(v) == 'table' then
      for k,v in ipairs(v) do
         print(k .. '---->' .. v)
      end
   else
      print(k .. '-->' ..v)
   end
end


print('Functions are first-class objects')
fn = function () print("I'm a first-class citizen!") end

fn()

function compare(a,b)
   return a == b
end

print('Comparing different types will result in an error')

if (pcall(compare('10', 10))) then
   print('"10" is equal to 10')
else
   print('"10" is not equal to 10')
end

print("shortcut evaluation: x = a or b is same as saying if a then x = a else x = b end")
a = 10
b = 30
-- if a > b then result is true
-- true and `a` results in the value of a, the `or` part is not evaluted
-- if a < b then the result is false, then the `and` part is not evaluated &
-- the `or` part is returned
max = (a > b) and a or b
print(max)

print('Multiple assignments are allowed')
i,j = 200,'cat'
print(i .. ' & ' ..j)

-- Closure
print("closures use upvalues to keep states between runs")
function counter()
   local i = 0
   return function()
      i = i + 1
      return i
   end
end

f1 = counter()
print(f1())
print(f1())
print(f1())
f2 = counter()
print(f2())

print('Parameters are also used as upvalues and retained across calls')
function mult(i)
   return function()
      i = i*i
      return i
   end
end

f1 = mult(2)
print(f1())
print(f1())
print(f1())

print('Creating an iterator')

function values(t)
   local i = 0
   return function() i = i + 1; return t[i] end
end

-- get the iterator object
t = {10, 20, 30 ,40}
iter = values(t)

-- 1. iterate manually
print('iterate manually')
do
   while true do
      local element = iter()
      if element == nil then break end
      print(element)
   end
end

print('iterate with generic for')
-- get new iterator
iter = values(t)
-- `for` does book-keeping for us
for element in values(t) do
   print(element)
end

print('iterate over all words from input')

-- Temporarily comment out
-- a function that returns an iterator
-- function allwords()
--    local line = io.read()
--    local pos = 1
--    return function()
--       while line do
--          local s, e = string.find(line, '%w+', pos)
--          if s then -- if we found a word
--             pos = e + 1
--             return string.sub(line, s, e)
--          else
--             line = io.read()
--             pos = 1
--          end
--       end
--       return nil
--    end
-- end

-- words = allwords()
-- for word in words do
--    print(word)
-- end

print('----- Stateless iterators ------')
-- the idea is that `for` can do the bookkeeping for us
-- and we need not create a new closure for each loop

a = {1, 2, 3, 4, 5, 6}

local function iter(a, i)
   i = i + 1
   local v = a[i]
   if v then
      return i, v
   end
end

local function ipairs(a)
    print('local ipairs()')
    return iter, a, 0
end

for i,v in ipairs(a) do
   print(i .. '--->' .. v)
end

print('--- call again without any state reset or having to create new closure ---')
for i,v in ipairs(a) do
   print(i .. '--->' .. v)
end

print('---- Linked list using stateless iterator -----')
-- Our next iterator
-- if node is nil then we return the head of the list else next element
local function getnext(list, node)
   return (not node and list) or node.next
end

function traverse(list)
   return getnext, list, nil
end

-- make a test list
vals = {"sally", "kitty", "hannah"}
linked_list = nil
for _,v in pairs(vals) do
   linked_list = {value=v, next=linked_list}
end

for node in traverse(linked_list) do
   print("value = " .. node.value .. " next = " .. tostring(node.next))
end

-- Iterator with complex state
-- Generally recommended to keep the iterator stateless and let
-- for loop maintain state information as seen below

-- check if a file exists
local file_exists = function (file)
   if file == nil then return nil end
   print('checking "' .. file .. '"')
   local f = io.open(file, "rb")
   if f then f:close() end
   return f~=nil
end

-- forward declare
local iterator

-- this is not ideal since we are opening file each time...but bear with me for now
local function allwords(file)
   if file_exists(file) then
      local f=io.open(file, 'rb')
      local state = {line=f:read('*l'), pos=1, f=f}
      return iterator, state
   else
      print('No such file, bail...')
      return function() return nil end
   end
end

function iterator(state)
   --print('line = ' .. state.line)
   while state.line do
      -- looks for next word
      local s, e = string.find(state.line, '%w+', state.pos)
      if s then
         -- update next position after this word
         state.pos = e + 1
         return string.sub(state.line, s, e)
      else
         state.line = state.f:read('*l')
         state.pos = 1
      end
   end
   print('Closing file ' .. tostring(state.f))
   state.f:close()
   return nil
end

for w in allwords('boo.txt') do
   print(w)
end

local buf = nil
for w in allwords('if.txt') do
   buf = (buf or '') .. ' ' .. w
end

print(buf)

-- Temporarily comment
-- print('------ True iterator -------')
-- -- the previous iterators are more like generators
-- -- in this case it's the iterator that does the iteration
-- local function allwords(f)
--    for line in io.lines() do
--       print('***')
--       for word in string.gmatch(line, '%w+') do
--          print('-- true iterator --')
--          f(word)
--       end
--    end
-- end

-- allwords(print)

-- Temporarily comment
-- print('Print something with a bunch of if\'s')
-- -- we could pass in any kind of function to the iterator
-- local count = 0
-- allwords(function(w) if string.lower(w) == 'if' then count = count + 1 end end)
-- print('Number of "if"s = ' .. count)

