# Who are you?

@hckrtst - I am a developer who is hungry to learn and grow

# What the heck is this?

The project is expected to be a comprehensive collection of topics
similar to those typically found in tech interviews?

# How do I join this group?

Send a pull request!

> Remember to add your name to this file

# What language can I use to solve the problems?

Any language you like. If you do use an obscure language then use plenty of comments.

# General Resources

* [Questions to ask employers](https://jvns.ca/blog/2013/12/30/questions-im-asking-in-interviews/)
* [LeetCode](leetcode.com)
* [Problem solving exercises](https://github.com/hckrtst/interactive-coding-challenges)
* [Algorithms and Data Structures with Python](http://interactivepython.org/runestone/static/pythonds/index.html)
* [Study Notes](notes/README.md)
* [Algorithms and data structures with Swift](https://github.com/raywenderlich/swift-algorithm-club)
* [Python interview problems](https://github.com/donnemartin/interactive-coding-challenges)
* [Embedded Systems Refresher](https://www.cs.cmu.edu/~213/schedule.html)
* [Talking about yourself in an interview](https://news.ycombinator.com/item?id=14218559)
